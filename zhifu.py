# -*- coding: utf-8 -*-
from flask import Flask
from src.common_tools.cryptos.zhifucloud import Encryptor
import logging.config
from src.common_tools.errors.signature_fail_error import SignatureFailure
import json

test = Flask(__name__)

encryptor = Encryptor("RSA")
logger = logging.getLogger("console")


@test.route('/')
def hello_world():
    return 'Hello World!'

@test.route('/notices/<notice>')
def notices(notice):
    resp_content = notice
    logger.debug(f"The notice content is: {resp_content}")

    resp_plain = encryptor.decrypt(resp_content)
    logger.info(f"The DECRYPTED notice is: {resp_plain}")

    # check the sign of response
    resp_json = json.loads(resp_plain)
    resp_head = resp_json["head"]
    resp_body = resp_json["body"]
    resp_sign = resp_json["sign"]

    (sign_content, sign) = encryptor.sign(resp_head, resp_body, "response")
    if sign != resp_sign:
        raise SignatureFailure(f"The signature {sign} is different from content {resp_sign}", resp_head, resp_body,
                               resp_sign, sign)
    result = {"success":"000000"}

    return result



if __name__ == '__main__':
    test.run(host='0.0.0.0')
# -*- coding: utf-8 -*-
import http.client
import hashlib
from datetime import datetime
import os
import uuid
import json
import urllib.parse
import logging.config
import base64
import M2Crypto
from faker import Faker
import pytest

from src.config_loader import ConfigLoader
from src.common_tools.networks.zhifucloud import NetBroker
from src.common_tools.cryptos.zhifucloud import Encryptor
from src.common_tools.tool import Tool
from src.common_tools.picture_tool import PictureTool
from tests.dict_templates import *

from locust import Locust, HttpLocust, TaskSet, task, events
import time


zhifucloud_broker = NetBroker()
zhifucloud_encryptor = Encryptor("RSA")
logger = logging.getLogger("console")
faker = Faker(locale="zh_CN")
tool = Tool()
configs = ConfigLoader().loadConfigs()

picture_tool = PictureTool()
dt = datetime.now()

company3 = { #先开户，后实名
    "enterprise_card_type": "01", #营业执照
    "enterprise_registration_no": f"{int(faker.pyfloat(left_digits=8, right_digits=0, positive=True))}-{int(faker.pyfloat(left_digits=1, right_digits=0, positive=True))}",
    "account_name": "深圳市亿诚电子科技有限公司", #固定测试挡板企业数据
    "acct": faker.credit_card_number(),
    "bank_code": "ICBC",
    "cust_merchant_no": str(uuid.uuid1()), #str(uuid.uuid1())  "64acf2f6-d0e5-11e8-8d30-ac2b6e261105"
    "cust_mobile": faker.phone_number(),
    "ent_name": "深圳市亿诚电子科技有限公司", #固定测试挡板企业数据
    "corporation_name": faker.name(),
    "corporation_id_no": faker.ssn(),
    "cust_type": "08", #企业客户
    "balance_acc_no":"3008001000880074000010",  #富民虚拟账户号 3008001000880074000010
    "cust_no": "",
    "is_real_name": False,
    "virtual_balances": {
        "ledger_balance" : 78027.40,
        "available_balance" : 78027.40,
        "frozen_balance": 0.0
    },
    "transaction_list": [] #交易列表
}


company1 = {
    'interface_type': '2',
    'balance_acc_no': '3008001000880157000010',
    'cust_merchant_no': 'ef9a3a5e-d2a7-11e8-b962-ac2b6e261105',
    'cust_type': '08',
    'enterprise_card_type': '01',
    'enterprise_registration_no': '27394704-9',
    'ent_name': '深圳市亿诚电子科技有限公司',
    'corporation_name': '莫亮',
    'corporation_id_no': '451030197012153843',
    'bank_code': 'ICBC',
    'account_name': '深圳市亿诚电子科技有限公司',
    'acct': '180051727738846',
    "is_real_name": True ,
    "cust_mobile": "15600991569",
    "virtual_balances": {
        "ledger_balance": 0.0,
        "available_balance": 0.0,
        "frozen_balance": 0.0
    },
    "transaction_list": [] #交易列表
}

company2 = { #直接实名开户
    "enterprise_card_type": "01", #营业执照
    "enterprise_registration_no": f"{int(faker.pyfloat(left_digits=8, right_digits=0, positive=True))}-{int(faker.pyfloat(left_digits=1, right_digits=0, positive=True))}",
    "account_name": "深圳市亿诚电子科技有限公司", #固定测试挡板企业数据
    "acct": faker.credit_card_number(),
    "bank_code": "ABC",
    "cust_merchant_no": str(uuid.uuid1()),
    "cust_mobile": faker.phone_number(),
    "ent_name": "深圳市亿诚电子科技有限公司", #固定测试挡板企业数据
    "corporation_name": faker.name(),
    "corporation_id_no": faker.ssn(),
    "cust_type": "08", #企业客户
    "balance_acc_no": "", #富民虚拟账户号
    "cust_no": "",
    "is_real_name": False,
    "business_type": "1",  # 必填枚举值：1：对公 2：对私
    "acct_type": "1",      #结算卡账户类型 1：本行账户 2:他行账户 3：互金账户
    "virtual_balances": {
        "ledger_balance" : 0.0,
        "available_balance" : 0.0,
        "frozen_balance": 0.0
    },
    "transaction_list": [] #交易列表
}

account = {
    "cust_merchant_no":"448f96a8-c2c7-11e8-ae51-8c8590b65d34",
    "balance_acc_no":"3008001000877963000010",
    "account_name":"深圳市亿诚电子科技有限公司",
}


real_account = {
    "interface_type": "2",
	"enterprise_card_type": "01",
	"enterprise_registration_no": "12920472-6",
	"account_name": "深圳市亿诚电子科技有限公司",
	"acct": "347310608746355",
	"bank_code": "ABC",
	"cust_merchant_no": "963a049f-d2b6-11e8-b962-ac2b6e261105",
	"cust_type": "08",
	"cust_mobile": "18985322464",
	"ent_name": "深圳市亿诚电子科技有限公司",
	"corporation_name": "成兵",
	"corporation_id_no": "340100197410095598",
    "cust_no": "3008001000880256",
    "balance_acc_no": "3008001000880256000010",
}



non_real_account_commpany = {
    "interface_type": "0",#非实名
    "cust_merchant_no": "ef9a3a5e-d2a7-11e8-b962-ac2b6e261105",#商户侧用户唯一 ID
    "cust_type": "08", #用户类型
    "cust_mobile": "18572393507", #注册手机号
    "cust_no": "3008001000880157", #会员号
    "balance_acc_no": "3008001000880157000010", #富民虚拟账户号
}

non_real_account_persion = {
	"interface_type": "0",
	"cust_merchant_no": "3aaaa4c8-d2aa-11e8-b962-ac2b6e261105",
	"cust_type": "00",
	"cust_mobile": "18996368987",
    "cust_no": "3000001000880163",
	"balance_acc_no": "3000001000880163000010",
    "bank_code": "CMB",
    "acct": "6214830171182081",
    "corporation_name":"王欢",
}







def test_rsa_encrypt():
    origin = '{"sign":"a719a5d73346b7438e6b8a1f327b4dcb","body":{"reqListDateMap":"[{\"corporation_name\":\"李小伟\",\"corporation_id_no\":\"511801199205230013\",\"cust_type\":\"08\",\"acct\":\"6228481028953823671\",\"cust_mobile\":\"18610083002\",\"interface_type\":\"2\",\"ent_name\":\"深圳市亿诚电子科技有限公司\",\"enterprise_card_type\":\"03\",\"cust_merchant_no\":1537499181639,\"enterprise_registration_no\":\"2223317633333\",\"bank_code\":\"ABC\",\"account_name\":\"测试\"}]"},"head":{"timestamp":"20170516120101","child_mer_id":"","mer_id":"0000000034","req_sn":"a1537499181639","service":"service.accountpayment.register","child_app_id":"","app_id":"0000000022"}}'

    encrtyped_content = zhifucloud_encryptor.encrypt(origin)

    logger.debug(f"The encrypt content is: {encrtyped_content}")
    assert len(encrtyped_content) > 10

def test_rsa_decrypt():
    cipher_text = "GK4k8ALJyGCYJC0E7fJKRvVGrdtOyu3J+ldRJSw1GKnqwbyOqLEpP2pqomNC7ZMAag6ydw4CX+DcyeZZX04luShJ9bkxWaPZ0wm9AMI92v6Qb5qaD9IXxUWen5U0X3wK9PZX/kX0PyYRr7PhszI8NeqbC4NDiWGniGVnS5K5IxUBZKtIuAkSxAjlQGMvkmTgpOmPpqQi4qIXZl9gbxqlDcMCbAdJNk8wQ17sA2++paZrbIPf7ZCm+wI9Nb9pIDCcXpJFhnXjpuWYkmwxt340x0ybDfUF1JS/Bq792DKMtVdf5SAl6hsBuQuedh2Cci3yR5OQpgWbRhCdkXyHb55bt3zdz2qWEqQRq3UP5RQmY3WHdKMt/DTySJMccDtL2T06mxdUJSCFTLtQfmCEm02XGV4MHB1k14S/6F9YCu18yBttdUYUgUhfZPf/Dj7LY0G6tj9a+YnyjPH3noX6JG+A+50hEv4OmirLqDxghyJujnU+F70typGN+kSid/cJUEBzqWwZjxFA8TgUFzWxCCtXPHujFJyoS5Sm59d0IWI/8lvkKCoxz/c3XwymviSQGgxfWA5Ib3WOSND76NlsvswdGf95e9ai7WAZO1cDamDBIknjtHiKtvN9B3v3JkMFr5Hd5UGQArKHnfjcIHSq3Le27MWeHEyiLXGJL+ZOjn521LBeXs1jfec79EsjO/2iUQSahZdqwlOaG4VUNadnWEjw0LU6VSrTkJkGdSTHT9wZmswzmqgDkpjWZnecofmiKC2t369fu19r8jqdqddESmfebUc7eQNGNniVSRrked725tvb/r/PJXkO0eSppjeEmG4dZ/UYL3ncwQjvlh8uuLZxfQ=="

    content = zhifucloud_encryptor.decrypt(cipher_text)
    logger.debug(f"The decrypted content {content}")
    plain_json = json.loads(content)
    assert len(plain_json["head"]) > 0
    assert len(plain_json["body"]) > 0
    assert len(plain_json["sign"]) > 0

#测试3.1 开户（实名，非实名） (通过)
def test_open_account():
    logger.info(f"In test_open_account------")
    """ """
    #company1 -- 非实名开户
    reqListDateMap = [{
        "interface_type": "0", #非实名
        "cust_merchant_no": company3["cust_merchant_no"],
        "cust_type": "08",
        "cust_mobile": company3["cust_mobile"],
    }]

    bodies = {
        "reqListDateMap":json.dumps(reqListDateMap)
    }

    response = zhifucloud_broker.request_endpoint("service.accountpayment.register",str(uuid.uuid1()).replace("-",""),bodies)
    response_json = json.loads(response)
    logger.debug(f"The response of company1 is {response_json}")

    assert response_json["body"]["res_code"] == "000000" #return successfully

    resListDateMap = json.loads(response_json["body"]["resListDateMap"])
    assert len(resListDateMap) == 1

    assert resListDateMap[0]["res_code"] == "0" # account open successfully

    company1["balance_acc_no"] = resListDateMap[0]["balance_acc_no"]
    company1["cust_no"] = resListDateMap[0]["cust_no"]
    assert len(company1["balance_acc_no"]) > 0

    logger.debug(f"The company1 is now: {company1}")

    """ -- failed since of the bank error   """
    #company2--实名开户
    reqListDateMap = [{
        "interface_type": "2",  # 企业实名
        "enterprise_card_type":company2["enterprise_card_type"],
        "enterprise_registration_no": company2["enterprise_registration_no"],
        "account_name": company2["account_name"],
        "acct": company2["acct"],
        "bank_code": company2["bank_code"],
        "cust_merchant_no": company2["cust_merchant_no"],
        "cust_type": "08",
        "cust_mobile": company2["cust_mobile"],
        "ent_name": company2["ent_name"],
        "corporation_name": company2["corporation_name"],
        "corporation_id_no": company2["corporation_id_no"],
        # "business_type": company2["business_type"],#必填枚举值：1：对公 2：对私
        # "acct_type": company2["acct_type"],        #结算卡账户类型 1：本行账户 2:他行账户 3：互金账户
    }]

    bodies = {
        "reqListDateMap": json.dumps(reqListDateMap)
    }

    logger.debug(f"Company2 : {company2}")

    response = zhifucloud_broker.request_endpoint("service.accountpayment.register", str(uuid.uuid1()).replace("-", ""),
                                                  bodies)
    response_json = json.loads(response)
    logger.info(f"The response of company2 is {response_json}")

    assert response_json["body"]["res_code"] == "000000"

    resListDateMap = json.loads(response_json["body"]["resListDateMap"])
    assert len(resListDateMap) == 1

    assert resListDateMap[0]["res_code"] == "0"  # account open successfully

    company2["balance_acc_no"] = resListDateMap[0]["balance_acc_no"]
    company2["cust_no"] = resListDateMap[0]["cust_no"]
    company2["is_real_name"] = True

    assert len(company2["balance_acc_no"]) > 0


    logger.debug(f"The company2 is now: {company2}")


#should execute after open account
#测试3.2 账户实名 (通过)
def test_real_name():
    #open account for company1
    real_name_request_dict = real_name_request_dict_template_3_2.copy()
    real_name_request_dict = tool.fillDictContent(real_name_request_dict, non_real_account_persion)
    """个人实名"""
    real_name_request_dict["interface_type"] = "0"
    real_name_request_dict["cust_type"] = "00"
    real_name_request_dict["cust_name"] = "王欢"
    real_name_request_dict["cust_idt"] = "0"
    real_name_request_dict["cust_id_no"] = "610302199107123056"
    real_name_request_dict["mobile_phone_num"] = "15600991569"
    logger.debug(f"The filled real_name_request_dict is {real_name_request_dict}")

    response = zhifucloud_broker.request_endpoint("service.accountpayment.certified", str(uuid.uuid1()).replace("-", ""),
                                                  real_name_request_dict)
    response_json = json.loads(response)

    assert response_json["body"]["res_code"] == "000000"  # return successfully
    company1["is_real_name"] = True
    return response

#3.3影像资料传输接口（通过）
def test_cust_image_save():

    cust_image_save_dict = cust_image_save_dict_template_3_3.copy();
    cust_image_save_dict = tool.fillDictContent(cust_image_save_dict, company1)

    # cust_image_save_dict["idcard_front"] = picture_tool.jpg_to_base64("/home/wang/Pictures/one.jpg")
    # cust_image_save_dict["idcard_back"] = picture_tool.jpg_to_base64("/home/wang/Pictures/two.jpg")

    cust_image_save_dict["business_license_photo"] = picture_tool.jpg_to_base64("/home/wang/Pictures/two.jpg")
    logger.debug(f"The filled cust_image_save_dict is {cust_image_save_dict}")

    response = zhifucloud_broker.request_endpoint("service.accountpayment.custimagesave",
                                                  str(uuid.uuid1()).replace("-", ""),
                                                  cust_image_save_dict)
    response_json = json.loads(response)

    assert response_json["body"]["res_code"] == "000000"




#测试3.4查询虚拟账号(通过)
def test_query_virtual_balance_no():
    balance_no_request_dict = virtual_balance_no_request_dict_template_3_4.copy()
    balance_no_request_dict = tool.fillDictContent(balance_no_request_dict, company1)

    response = zhifucloud_broker.request_endpoint("service.accountpayment.accountquery",
                                                  str(uuid.uuid1()).replace("-", ""),
                                                  balance_no_request_dict)

    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"  # return successfully
    assert response_json["body"]["cust_merchant_no"] == company1["cust_merchant_no"]
    return response


#测试3.5余额查询，以及3.9 上下账(测试通过)
def test_money_up_down():
    balance_query_request_dict = balance_query_request_dict_template_3_5.copy()
    balance_query_request_dict = tool.fillDictContent(balance_query_request_dict, company1)

    balance_up_request_dict = balance_money_change_request_dict_template_3_10.copy()
    balance_up_request_dict = tool.fillDictContent(balance_up_request_dict, company1)

    """ #query the balance before   """
  

    response = zhifucloud_broker.request_endpoint("service.accountpayment.querybalance",
                                                  str(uuid.uuid1()).replace("-", ""),
                                                  balance_query_request_dict)

    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"  # return successfully
    assert response_json["body"]["balance_acc_no"] == company1["balance_acc_no"]
    assert response_json["body"]["funds_held"] == "0.00"
    assert response_json["body"]["available_bal"] == "0.00"
    assert response_json["body"]["ledger_bal"] == "0.00"

    company1["virtual_balances"]["ledger_balance"] = float(response_json["body"]["ledger_bal"])
    company1["virtual_balances"]["available_balance"] = float(response_json["body"]["available_bal"])
    company1["virtual_balances"]["frozen_balance"] = float(response_json["body"]["funds_held"])

    """ #do the money up, 上账 """
    balance_up_request_dict = balance_money_change_request_dict_template_3_10.copy()
    balance_up_request_dict = tool.fillDictContent(balance_up_request_dict, company1)
    balance_up_request_dict["mer_order_no"] = str(uuid.uuid1()).replace("-", "")
    money_up = faker.pyfloat(left_digits=5, right_digits=2, positive=True)

    balance_up_request_dict["amount"] = f"{money_up:.2f}"
    balance_up_request_dict["origin_type"] = "1" #上账

    response = zhifucloud_broker.request_endpoint("service.accountpayment.accountrecharge",
                                                  str(uuid.uuid1()).replace("-", ""),
                                                  balance_up_request_dict)

    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"  # return successfully
    assert len(response_json["body"]["order_no"]) > 10
    # assert len(response_json["body"]["trade_no"]) > 10
    company1["transaction_list"].append(response_json["body"]["order_no"]) #留待回单

    #上账后查询余额
    response = zhifucloud_broker.request_endpoint("service.accountpayment.querybalance",
                                                  str(uuid.uuid1()).replace("-", ""),
                                                  balance_query_request_dict)

    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"  # return successfully
    assert response_json["body"]["balance_acc_no"] == company1["balance_acc_no"]
    assert response_json["body"]["funds_held"] == "0.00"
    assert response_json["body"]["available_bal"] == f"{money_up:.2f}"
    assert response_json["body"]["ledger_bal"] == f"{money_up:.2f}"

    company1["virtual_balances"]["ledger_balance"] += money_up #update the company money info
    company1["virtual_balances"]["available_balance"] += money_up

    print(money_up)
    return response

    """
    #下账, substract the money
    balance_up_request_dict["mer_order_no"] = str(uuid.uuid1()).replace("-", "")
    money_down = faker.pyfloat(left_digits=3, right_digits=2, positive=True)

    balance_up_request_dict["amount"] = f"{money_down:.2f}"
    balance_up_request_dict["origin_type"] = "2"  # 下账

    response = zhifucloud_broker.request_endpoint("service.accountpayment.accountrecharge",
                                                  str(uuid.uuid1()).replace("-", ""),
                                                  balance_up_request_dict)

    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"  # return successfully
    assert len(response_json["body"]["order_no"]) > 10
    # assert len(response_json["body"]["trade_no"]) > 10
    company1["transaction_list"].append(response_json["body"]["order_no"])  # 留待回单


    # 下账后查询余额
    response = zhifucloud_broker.request_endpoint("service.accountpayment.querybalance",
                                                  str(uuid.uuid1()).replace("-", ""),
                                                  balance_query_request_dict)

    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"  # return successfully
    assert response_json["body"]["balance_acc_no"] == company1["balance_acc_no"]
    assert response_json["body"]["funds_held"] == "0.00"
    assert response_json["body"]["available_bal"] == f"{money_up-money_down:.2f}"
    assert response_json["body"]["ledger_bal"] == f"{money_up-money_down:.2f}"

    company1["virtual_balances"]["ledger_balance"] -= money_down  # update the company money info
    company1["virtual_balances"]["available_balance"] -= money_down """

#3.6 查询账户的收支明细记录(通过)
def test_balance_change_record_query():
    balance_change_record_query_dict = balance_change_record_query_dict_template_3_6.copy();
    balance_change_record_query_dict = tool.fillDictContent(balance_change_record_query_dict, company1)
    balance_change_record_query_dict["client_serial_no"] = str(uuid.uuid1()).replace("-","")
    balance_change_record_query_dict["trade_end_date"] =  dt.strftime("%Y-%m-%d")
    logger.debug(f"查询账户的收支明细记录入参 {balance_change_record_query_dict} ")
    response = zhifucloud_broker.request_endpoint("service.accountpayment.balancechangerecordquery",
                                                  str(uuid.uuid1()).replace("-", ""),
                                                  balance_change_record_query_dict)
    response_json = json.loads(response)

    assert response_json["body"]["res_code"] == "000000"
    assert len(response_json["body"]["data"])>0
    data_list = json.loads(response_json["body"]["data"])
    assert len(data_list) > 0



#3.8冻结解冻(通过)
def test_money_frozen():

    """
     查帐
    """
    balance_query_request_dict = balance_query_request_dict_template_3_5.copy()
    balance_query_request_dict = tool.fillDictContent(balance_query_request_dict, company1)

    response = zhifucloud_broker.request_endpoint("service.accountpayment.querybalance",
                                                  str(uuid.uuid1()).replace("-", ""),
                                                  balance_query_request_dict)

    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"  # return successfully
    assert response_json["body"]["balance_acc_no"] == company1["balance_acc_no"]

    ledger_balance =  float(response_json["body"]["ledger_bal"]) # 账面金额
    available_balance = float(response_json["body"]["available_bal"]) # 可用余额
    funds_held = float(response_json["body"]["funds_held"]) # 冻结金额

    balance_money_freeze_request_dict = balance_money_freeze_request_dict_template_3_8.copy()
    balance_money_freeze_request_dict = tool.fillDictContent(balance_money_freeze_request_dict , company1)

    money_frozen = faker.pyfloat(left_digits=2, right_digits=2, positive=True)

    funds_held += money_frozen
    available_balance = available_balance - money_frozen

    balance_money_freeze_request_dict["frozenAmount"] = f"{money_frozen:.2f}"
    balance_money_freeze_request_dict["frozenType"] = "1" #冻结类型 1 | 永久， 2 | 临时
    balance_money_freeze_request_dict["implementFlag"] = "0"  #0 | 立即执行，1 | 定时执行
    balance_money_freeze_request_dict["currency"] = "1" #币种
    balance_money_freeze_request_dict["frozenStartTime"] = "20270122"  # 时间格式：yyyymmdd
    balance_money_freeze_request_dict["frozenEndTime"] = "20270123" # 时间格式：yyyymmdd
    balance_money_freeze_request_dict["frozenReason"] = "测试"

    balance_money_freeze_request_dict["client_serial_no"] = str(uuid.uuid1()).replace("-","")



    logger.debug(f"冻结 入参 {balance_money_freeze_request_dict}")

    response = zhifucloud_broker.request_endpoint("service.accountpayment.capitalfrozen",
                                         str(uuid.uuid1()).replace("-",""),
                                         balance_money_freeze_request_dict)

    response_json = json.loads(response)

    assert response_json["body"]["res_code"] == "000000"
    assert len(response_json["body"]["data"]) > 0
    amtFlowList = json.loads(response_json["body"]["data"])
    assert len(amtFlowList["amtFlowList"]) > 0


    balance_query_request_dict = balance_query_request_dict_template_3_5.copy()
    balance_query_request_dict = tool.fillDictContent(balance_query_request_dict, company1)
    """  """
    response = zhifucloud_broker.request_endpoint("service.accountpayment.querybalance",
                                                  str(uuid.uuid1()).replace("-", ""),
                                                  balance_query_request_dict)

    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"  # return successfully
    assert response_json["body"]["balance_acc_no"] == company1["balance_acc_no"]

    assert ledger_balance == float(response_json["body"]["ledger_bal"])  # 账面金额
    assert available_balance == float(response_json["body"]["available_bal"])  # 可用余额
    assert funds_held == float(response_json["body"]["funds_held"])  # 冻结金额

    balance_money_freeze_request_dict["interface_type"] = 1
    balance_money_freeze_request_dict["thawrozenFlag"] = 0
    logger.debug(f"解冻 入参 {balance_money_freeze_request_dict}")

    response = zhifucloud_broker.request_endpoint("service.accountpayment.capitalfrozen",
                                         str(uuid.uuid1()).replace("-",""),
                                         balance_money_freeze_request_dict)
    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"
    assert len(response_json["body"]["data"]) > 0
    amtFlowList = json.loads(response_json["body"]["data"])
    assert len(amtFlowList["amtFlowList"]) > 0



    response = zhifucloud_broker.request_endpoint("service.accountpayment.querybalance",
                                                  str(uuid.uuid1()).replace("-", ""),
                                                  balance_query_request_dict)

    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"  # return successfully
    assert response_json["body"]["balance_acc_no"] == company1["balance_acc_no"]

    funds_held -= money_frozen
    available_balance += money_frozen

    assert ledger_balance == float(response_json["body"]["ledger_bal"])  # 账面金额
    assert available_balance == float(response_json["body"]["available_bal"])  # 可用余额
    assert funds_held == float(response_json["body"]["funds_held"])  # 冻结金额





#3.11虚拟账户提现 (通过)
def test_withdraw():
    # return #等待接口问题解决

    withdraw_request_dict = withdraw_request_dict_template_3_11.copy()
    # withdraw_request_dict = tool.fillDictContent(withdraw_request_dict,company1)
    withdraw_request_dict = tool.fillDictContent(withdraw_request_dict,non_real_account_persion)


    withdraw_request_dict["mer_order_no"] = str(uuid.uuid1()).replace("-", "")

    amount = faker.pyfloat(left_digits=2, right_digits=2, positive=True)
    withdraw_request_dict["amount"] = f"{amount:.2f}"
    withdraw_request_dict["account_in"] = non_real_account_persion["acct"]
    withdraw_request_dict["name_in"] = non_real_account_persion["corporation_name"]
    withdraw_request_dict["trade_remark"] = "提现测试"
    withdraw_request_dict["cust_no_in"] =non_real_account_persion["cust_merchant_no"] # str(uuid.uuid1()).replace("-", "")



    logger.info(f"company1: {company1}")
    response = zhifucloud_broker.request_endpoint("service.accountpayment.withdrawals",
                                           str(uuid.uuid1()).replace("-", ""),
                                           withdraw_request_dict)

    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"  # return successfully

    #check the balance
    balance_query_request_dict = balance_query_request_dict_template_3_5.copy()
    balance_query_request_dict = tool.fillDictContent(balance_query_request_dict, company1)

    response = zhifucloud_broker.request_endpoint("service.accountpayment.querybalance",
                                           str(uuid.uuid1()).replace("-", ""),
                                           balance_query_request_dict)

    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"  # return successfully
    assert response_json["body"]["balance_acc_no"] == company1["balance_acc_no"]
    assert response_json["body"]["funds_held"] == "0.00"
    assert response_json["body"]["available_bal"] == f"{company1['virtual_balances']['available_balance']-amount:.2f}"
    assert response_json["body"]["ledger_bal"] == f"{company1['virtual_balances']['ledger_balance']-amount:.2f}"

    #update balance
    company1['virtual_balances']['available_balance'] -= amount
    company1['virtual_balances']['ledger_balance'] -= amount

    #store the order no
    company1["transaction_list"].append(response_json["body"]["order_no"])

#3.7 电子回单(验证通过)
def test_receipt():
    receit_request_dict = receipt_request_dict_template_3_7.copy()
    #fill in the basic info and the transaction info
    receit_request_dict = tool.fillDictContent(receit_request_dict, company1)

    #check if the path exists, else create one
    if not os.path.exists(configs["receipt-path"]):
     os.makedirs(configs["receipt-path"])
    company1["transaction_list"].append("szxf1539861912513")
    assert len(company1["transaction_list"])>0 #has got transactions
    for tran in company1["transaction_list"]:
        receit_request_dict["trans_flow_no"] = tran

    response = zhifucloud_broker.request_endpoint("service.accountpayment.getvoucher",
                                                   str(uuid.uuid1()).replace("-", ""),
                                                   receit_request_dict)

    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"  # return successfully
    pdf_content = response_json["body"]["base_content"]
    logger.info(f"The pdf content in base64 mode: {pdf_content}")

    #write the content to pdf file
    with open(f"{configs['receipt-path']}/{tran}.pdf", 'wb') as f:
        f.write(base64.b64decode(pdf_content))

# 3.12 上账模式消费（成功）
def test_consume_count():
    """ """
    # 3.12上账模式消费
    consumption_fee_dict = consumption_fee_dict_template_3_12.copy()
    consumption_fee_dict = tool.fillDictContent(consumption_fee_dict,company1)
    consumption_fee_dict["mer_order_no"] = str(uuid.uuid1()).replace("-","")
    consumption_fee_dict["cust_no_in"] = "448f96a8-c2c7-11e8-ae51-8c8590b65d34"
    consumption_fee_dict["account_in"] = "3008001000877963000010"
    amount = faker.pyfloat(left_digits=2, right_digits=2, positive=True)
    consumption_fee_dict["amount"] = f"{amount:.2f}"
    poundage_amount = faker.pyfloat(left_digits=1, right_digits=2, positive=True)
    consumption_fee_dict["poundage_amount"] = f"{poundage_amount:.2f}"
    consumption_fee_dict["purpose"] = "1002" #虚拟商品购买

    logger.debug(f"The filled consumption_fee_dict  is {consumption_fee_dict}")
    response = zhifucloud_broker.request_endpoint("service.accountpayment.consumptionfee",str(uuid.uuid1()).replace("-",""),consumption_fee_dict)

    response_json = json.loads(response)

    assert response_json["body"]["res_code"] == "000000"
    assert len(response_json["body"]["trade_no"]) >0






#3.16 满足商户查询到对应交易记录(通过)
def test_trade_detail_query(mer_order_no,order_no):
    trade_detail_query_dict = trade_detail_query_dict_template_3_16.copy()
    trade_detail_query_dict["mer_order_no"]=mer_order_no
    trade_detail_query_dict["order_no"] = order_no
    logger.debug(f"The filled trade_detail_query_dict is {trade_detail_query_dict}")
    response = zhifucloud_broker.request_endpoint("service.accountpayment.tradedetailquery",
                                                  str(uuid.uuid1()).replace("-", ""), trade_detail_query_dict)
    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"
    assert len(response_json["body"]["trade_no"]) > 0

#todo 郭云测试不通 明天测
#3.15 消费退款接口
def test_consume_refund(original_trade_no,amount = 0.0):
    consume_refund_dict = consume_refund_dict_template_3_15.copy()
    consume_refund_dict = tool.fillDictContent(consume_refund_dict,company1)
    consume_refund_dict["mer_order_no"] = str(uuid.uuid1()).replace("-","")
    consume_refund_dict["original_trade_no"] = original_trade_no
    consume_refund_dict["amount"] = amount

    logger.debug(f"The filled consume_refund_dict is {consume_refund_dict}")
    response = zhifucloud_broker.request_endpoint("service.accountpayment.consumerefund",str(uuid.uuid1()).replace("-",""),consume_refund_dict)
    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"
    assert len(response_json["body"]["trade_no"]) > 0



#3.17 商户交易分账(通过)
def test_consume_ledger():

    balance_query_request_dict = balance_query_request_dict_template_3_5.copy()
    balance_query_request_dict = tool.fillDictContent(balance_query_request_dict, company1)

    # 分帐前查询余额
    response = zhifucloud_broker.request_endpoint("service.accountpayment.querybalance",
                                                  str(uuid.uuid1()).replace("-", ""),
                                                  balance_query_request_dict)

    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"  # return successfully
    assert response_json["body"]["balance_acc_no"] == company1["balance_acc_no"]

    funds_held = float(response_json["body"]["funds_held"])
    available_bal =  float(response_json["body"]["available_bal"])
    ledger_bal =  float(response_json["body"]["ledger_bal"] )

    """#3.17 商户交易分账 开始 """
    consume_ledger_dict = consume_ledger_dict_template_3_17.copy()

    consume_ledger_dict["mer_order_no"] = str(uuid.uuid1()).replace("-","")
    consume_ledger_dict["cust_merchant_out"] = company1["cust_merchant_no"]
    consume_ledger_dict["account_out"] = company1["balance_acc_no"]

    consume_ledger_dict["cust_type_out"] = "08"

    amount = faker.pyfloat(left_digits=3, right_digits=2, positive=True)
    consume_ledger_dict["amount"] = f"{amount:.2f}"
    poundage_amount = faker.pyfloat(left_digits=2,right_digits=2,positive=True)
    consume_ledger_dict["poundage_amount"] = f"{poundage_amount:.2f}"

    consume_ledger_dict["guarantee"] = "true"

    ledger_account_info = ledger_account_info_dict_template_3_17.copy()
    ledger_account_info["cust_merchant_in"] = account["cust_merchant_no"]
    ledger_account_info["account_in"] = account["balance_acc_no"]
    ledger_account_info["cust_type"] = "08"

    ledger_account_info["ledger_amount"] = f"{amount:.2f}"
    ledger_account_info["fee_amount"] = f"{poundage_amount:.2f}"

    ledger_account_infos = []
    ledger_account_infos.append(ledger_account_info)

    consume_ledger_dict["ledger_account_info"] = json.dumps(ledger_account_infos)

    fee_splitting_info = fee_splitting_info_dict_template_3_17.copy()
    fee_splitting_info["cust_merchant_in"] = account["cust_merchant_no"]
    fee_splitting_info["account_in"] = account["balance_acc_no"]
    fee_splitting_info["cust_type"] = "08"
    fee_splitting_info["splitting_amount"] = f"{poundage_amount:.2f}"

    fee_splitting_infos = []
    fee_splitting_infos.append(fee_splitting_info)
    consume_ledger_dict["fee_splitting_info"] = json.dumps(fee_splitting_infos)
    logger.debug("The filled consume_ledger_dict is {consume_ledger_dict} ")

    response = zhifucloud_broker.request_endpoint("service.accountpayment.consumeLedger",str(uuid.uuid1()).replace("-",""),consume_ledger_dict)

    response_json = json.loads(response)

    assert response_json["body"]["res_code"] == "000000"
    assert len(response_json["body"]["trade_no"]) > 0
    assert len(response_json["body"]["mer_order_no"]) > 0

    trade_no = response_json["body"]["trade_no"]

    available_bal -= amount

    ledger_bal -= amount

    # # 分账后查询余额
    # response = zhifucloud_broker.request_endpoint("service.accountpayment.querybalance",
    #                                               str(uuid.uuid1()).replace("-", ""),
    #                                               balance_query_request_dict)
    #
    # response_json = json.loads(response)
    # assert response_json["body"]["res_code"] == "000000"  # return successfully
    # assert response_json["body"]["balance_acc_no"] == company1["balance_acc_no"]
    #
    # assert response_json["body"]["funds_held"] == funds_held
    # assert response_json["body"]["ledger_bal"] == ledger_bal
    # assert response_json["body"]["available_bal"] == available_bal

    # balance_query_request_dict = tool.fillDictContent(balance_query_request_dict, account)
    # 分账后查入账余额
    # response = zhifucloud_broker.request_endpoint("service.accountpayment.querybalance",
    #                                               str(uuid.uuid1()).replace("-", ""),
    #                                               balance_query_request_dict)
    #
    # response_json = json.loads(response)
    # assert response_json["body"]["res_code"] == "000000"  # return successfully
    # assert response_json["body"]["balance_acc_no"] == company1["balance_acc_no"]
    #
    # assert response_json["body"]["funds_held"] == amount

    #商户消费担保确认接口
    confirmation_consumption_call(trade_no,consume_ledger_dict["cust_merchant_out"],"08")



#3.18 商户消费担保确认接口
def confirmation_consumption_call(trade_no,cust_merchant_out,cust_type_in):
    confirmation_consumption_call_dict = confirmation_consumption_call_dict_template_3_18.copy()
    confirmation_consumption_call_dict["mer_order_no"] = str(uuid.uuid1()).replace("-","")
    confirmation_consumption_call_dict["trade_no"] = trade_no
    confirmation_consumption_call_dict["cust_merchant_out"] = cust_merchant_out
    confirmation_consumption_call_dict["cust_type_in"] = cust_type_in
    logger.debug(f"The filled confirmation_consumption_call_dict is {confirmation_consumption_call_dict}")\

    response = zhifucloud_broker.request_endpoint("service.accountpayment.confirmationconsumptioncall",str(uuid.uuid1()).replace("-",""),confirmation_consumption_call_dict)

    response_json = json.loads(response)

    assert response_json["body"]["res_code"] == "000000"

    assert len(response_json["body"]["trade_no"]) > 0


#3.19 虛户个人 企业实名（仅实名）(通过)
def test_weak_certified():
    weak_certified_dict = weak_certified_dict_template_3_19.copy()
    weak_certified_dict = tool.fillDictContent(weak_certified_dict,non_real_account_persion)
    weak_certified_dict["cust_mobile"] = "15600991569" #faker.phone_number()
    weak_certified_dict["cust_name"] = "王欢" #faker.name()
    weak_certified_dict["cust_id_no"] = "610302199107123056"

    logger.debug(f"The filled weak_certified_dict is {weak_certified_dict}")

    response = zhifucloud_broker.request_endpoint("service.accountpayment.weakcertified",str(uuid.uuid1()).replace("-",""),weak_certified_dict)
    response_json = json.loads(response)

    assert response_json["body"]["res_code"] == "000000"

#3.21 转账 (通过)
def test_transfer():
    transfer_dict = transfer_dict_template_3_21.copy()
    transfer_dict = tool.fillDictContent(transfer_dict,company1)
    transfer_dict["mer_order_no"] = str(uuid.uuid1()).replace("-","")
    transfer_dict["name_out"] = company1["account_name"]
    transfer_dict["cust_no_in"] = real_account["cust_merchant_no"]
    transfer_dict["name_in"] = real_account["ent_name"]
    transfer_dict["account_in"] = real_account["balance_acc_no"]
    transfer_dict["acct_flag"] = "1"
    transfer_dict["amount"] = "10.01"
    logger.debug(f"The filled transfer_dict is {transfer_dict}")

    response = zhifucloud_broker.request_endpoint("service.accountpayment.transfer",str(uuid.uuid1()).replace("-",""),transfer_dict)
    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"


# 3.22消费接口(通过)
def test_consume():
    # 账户查询
    balance_query_request_dict = balance_query_request_dict_template_3_5.copy()
    balance_query_request_dict = tool.fillDictContent(balance_query_request_dict, company1)

    response = zhifucloud_broker.request_endpoint("service.accountpayment.querybalance",
                                                  str(uuid.uuid1()).replace("-", ""),
                                                  balance_query_request_dict)

    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"  # return successfully
    assert response_json["body"]["balance_acc_no"] == company1["balance_acc_no"]

    ledger_balance = float(response_json["body"]["ledger_bal"])  # 账面金额
    available_balance = float(response_json["body"]["available_bal"])  # 可用余额
    funds_held = float(response_json["body"]["funds_held"])  # 冻结金额


    consume_dict = consume_dict_template_3_22.copy()
    consume_dict = tool.fillDictContent(consume_dict , company1)
    consume_dict["mer_order_no"] = str(uuid.uuid1()).replace("-","")

    amount = faker.pyfloat(left_digits=3, right_digits=2, positive=True)
    consume_dict["amount"] = f"{amount:.2f}"
    consume_dict["purpose"] = 1001
    logger.debug(f"The filled consume_dict is {consume_dict}")
    response = zhifucloud_broker.request_endpoint("service.accountpayment.consume",str(uuid.uuid1()).replace("-",""),consume_dict)
    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"
    assert len(response_json["body"]["trade_no"]) > 0

    mer_order_no = response_json["body"]["mer_order_no"]
    order_no = response_json["body"]["order_no"]

    ledger_balance -= amount
    available_balance -= amount

    #账户查询
    response = zhifucloud_broker.request_endpoint("service.accountpayment.querybalance",
                                                  str(uuid.uuid1()).replace("-", ""),
                                                  balance_query_request_dict)

    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"  # return successfully
    assert response_json["body"]["balance_acc_no"] == company1["balance_acc_no"]

    assert ledger_balance == float(response_json["body"]["ledger_bal"])  # 账面金额
    assert available_balance == float(response_json["body"]["available_bal"])  # 可用余额
    assert funds_held == float(response_json["body"]["funds_held"])  # 冻结金额


    """
    # 3.13消费撤销接口
    cancel_consume_dict = cancel_consume_dict_template_3_13.copy()
    cancel_consume_dict = tool.fillDictContent(cancel_consume_dict, company1)
    cancel_consume_dict["mer_order_no"] = str(uuid.uuid1()).replace("-", "")
    cancel_consume_dict["original_trade_no"] = mer_order_no  # 原商户订单号

    logger.debug(f"The filled cancel_consume_dict  is {cancel_consume_dict}")
    response = zhifucloud_broker.request_endpoint("service.accountpayment.consumerevoke",
                                                  str(uuid.uuid1()).replace("-", ""), cancel_consume_dict)
    response_json = json.loads(response)

    assert response_json["body"]["res_code"] == "000000"
    assert len(response_json["body"]["trade_no"]) > 0"""

    # 消费冲正接口
    consume_reversal_dict = consume_reversal_dict_template_3_14.copy()
    consume_reversal_dict = tool.fillDictContent(consume_reversal_dict, company1)
    consume_reversal_dict["mer_order_no"] = str(uuid.uuid1()).replace("-", "")
    consume_reversal_dict["original_trade_no"] = mer_order_no

    logger.debug(f"The filled consume_reversal_dict  is {consume_reversal_dict}")
    response = zhifucloud_broker.request_endpoint("service.accountpayment.consumereversal",
                                                  str(uuid.uuid1()).replace("-", ""), consume_reversal_dict)

    response_json = json.loads(response)

    assert response_json["body"]["res_code"] == "000000"
    assert len(response_json["body"]["trade_no"]) > 0
    assert len(response_json["body"]["mer_order_no"]) > 0
    assert len(response_json["body"]["order_no"]) > 0

    ledger_balance += amount
    available_balance += amount




     #账户查询
    response = zhifucloud_broker.request_endpoint("service.accountpayment.querybalance",
                                                  str(uuid.uuid1()).replace("-", ""),
                                                  balance_query_request_dict)

    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"  # return successfully
    assert response_json["body"]["balance_acc_no"] == company1["balance_acc_no"]

    assert ledger_balance == float(response_json["body"]["ledger_bal"])  # 账面金额
    assert available_balance == float(response_json["body"]["available_bal"])  # 可用余额
    assert funds_held == float(response_json["body"]["funds_held"])  # 冻结金额



    # 3.16 查询
    test_trade_detail_query(mer_order_no, order_no)




#4.1 短信验证码发送接口
def test_send_msg_code():
    send_msg_code_dict = send_msg_code_dict_template_4_1.copy()
    send_msg_code_dict = tool.fillDictContent(send_msg_code_dict,company1)
    send_msg_code_dict["mer_order_no"] = str(uuid.uuid1()).replace("-", "")
    send_msg_code_dict["operate_type"] = "18"

    logger.debug(f"The filled send_msg_code_dict is {send_msg_code_dict}")

    response = zhifucloud_broker.request_endpoint("service.accountpayment.sendmsgcode",str(uuid.uuid1()).replace("-",""),send_msg_code_dict)
    response_json = json.loads(response)
    assert response_json["body"]["res_code"] == "000000"
    assert len(response_json["body"]["active_code_serial_no"]) > 0


class ZhifuTestSet(TaskSet):
    def on_start(self):
        print("--------on start-------")
        logger.info("-------on start")
        # test_open_account()
    #
    # tasks = {
    #     test_real_name: 1,
    #     test_money_up_down: 10,
    #     test_query_virtual_balance_no: 20
    # }

    @task(1)
    def real_name(self):
        start_time = time.time()
        try:
            # resp = test_real_name()
            resp = "real_name test result "
            logger.info(f"resp -- real_name")
        except Exception as e:
            total_time = int((time.time() - start_time) * 1000)
            events.request_failure.fire(request_type="zhifu_cloud", name="real_name", response_time=total_time, exception=e)
        else:
            total_time = int((time.time() - start_time) * 1000)
            events.request_success.fire(request_type="zhifu_cloud", name="real_name", response_time=total_time, response_length=len(resp))

    @task(10)
    def money_up_down(self):
        start_time = time.time()
        try:
            resp = test_money_up_down()
        except Exception as e:
            total_time = int((time.time() - start_time) * 1000)
            events.request_failure.fire(request_type="zhifu_cloud", name="virtual money up/down",
                                        response_time=total_time, exception=e)
        else:
            total_time = int((time.time() - start_time) * 1000)
            events.request_success.fire(request_type="zhifu_cloud", name="virtual money up/down",
                                        response_time=total_time, response_length=len(resp))

    @task(20)
    def test_query_balance(self):
        start_time = time.time()
        try:
            resp = test_query_virtual_balance_no()
        except Exception as e:
            total_time = int((time.time() - start_time) * 1000)
            events.request_failure.fire(request_type="zhifu_cloud", name="query_balance",
                                        response_time=total_time, exception=e)
        else:
            total_time = int((time.time() - start_time) * 1000)
            events.request_success.fire(request_type="zhifu_cloud", name="query_balance",
                                        response_time=total_time, response_length=len(resp))


class ZhifuLoadRunner(Locust):
    task_set = ZhifuTestSet
    min_wait = 2000
    max_wait = 5000





if __name__ == '__main__':
    test_open_account()
    # test_real_name()
    # test_query_virtual_balance_no()
    # test_money_up_down()
    # test_withdraw()
    # test_receipt()

    # test_rsa_encrypt()
    # test_rsa_decrypt()

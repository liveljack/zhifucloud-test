# -*- coding: utf-8 -*-
import logging

from src.common_tools.tool import Tool
from src.config_loader import ConfigLoader

tool = Tool()


def test_fillDictContent():
    tar_dict = {
        "f1" : "",
        "f2" : None,
        "f3" : "23",
        "f4" : 256,
        "f5" : True
    }

    src_dict = {
        "f1": "src",
        "f2": 123,
        "f3": "src23",
        "f4": 887,
        "f5": False,
        "f6": 2948.08
    }

    field_list = ["f3","f5"]

    filled_dict = tool.fillDictContent(tar_dict,src_dict)
    print(f"filled dict: {filled_dict}")
    assert filled_dict["f1"] == src_dict["f1"]
    assert filled_dict["f2"] == src_dict["f2"]

    assert filled_dict["f3"] == tar_dict["f3"]
    assert filled_dict["f3"] != src_dict["f3"]

    filled_dict = tool.fillDictContent(tar_dict, src_dict, field_list)

    print(f"filled dict: {filled_dict}")
    assert filled_dict["f3"] == src_dict["f3"]
    assert filled_dict["f5"] == src_dict["f5"]

    assert filled_dict["f4"] == tar_dict["f4"]


def test_removeNoneItemfromDict():
    test_dict = {
        "f1": None,
        "f2": "test",
        "f3": None,
        "f4": None,
    }

    ret_dict = tool.removeNoneItemfromDict(test_dict)
    assert len(test_dict) == 4
    assert len(ret_dict) == 1
    assert "f2" in list(ret_dict)
    assert "f1" not in list(ret_dict)


if __name__ == '__main__':
    test_fillDictContent()

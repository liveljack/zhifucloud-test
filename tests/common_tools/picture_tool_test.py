# -*- coding: utf-8 -*-
from common_tools.picture_tool import PictureTool


picture_tool = PictureTool()

def jpg_to_base64_test():
    pic_url = "image/two.jpg"
    base64_str = picture_tool.jpg_to_base64(pic_url)
    assert len(base64_str) > 0

if __name__ == '__main__':
    jpg_to_base64_test()
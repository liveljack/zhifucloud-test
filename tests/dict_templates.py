# -*- coding: utf-8 -*-
real_name_request_dict_template_3_2 = {
    "interface_type": "2", #企业
    "balance_acc_no": None,
    "cust_merchant_no": None,
    "cust_type": "08", #企业
    "enterprise_card_type": None,
    "enterprise_registration_no": None,
    "ent_name": None,
    "corporation_name": None,
    "corporation_id_no": None,
    "bank_code": None,
    "account_name": None,
    "acct": None,
}

cust_image_save_dict_template_3_3 = {
    "interface_type" : "2", # 0,个人资料传输 2，企业资料传输
    "cust_merchant_no": None,
    "idcard_front" : None,# 身份证 正面
    "idcard_back" : None,# 身份证 反面
    "business_license_photo" : None, # 营业执照
}

virtual_balance_no_request_dict_template_3_4 = {
    "cust_merchant_no": None,
    "cust_type": "08", #企业类型
}

balance_query_request_dict_template_3_5 = {
    "cust_merchant_no": None,
    "balance_acc_no": None,
    "cust_type": "08",
}

balance_change_record_query_dict_template_3_6 = {
    "client_serial_no":None,
    "page_no" : "1",
    "page_size" : "10",
    "balance_acc_no" : None,
    "trade_start_date" : "1970-01-01", #时间格式：yyyy-mm-dd
    "trade_end_date" : None, #时间格式：yyyy-mm-dd
}


receipt_request_dict_template_3_7 = {
    "cust_merchant_no": None, #商户侧id
    "balance_acc_no": None, #虚拟账户号
    "trans_flow_no": None, #交易流水号
    "cust_type": "08", #企业
}


balance_money_freeze_request_dict_template_3_8 = { #3.8虚拟账户资金冻结
    "client_serial_no":None,
    "interface_type": "0", #0-冻结,1-解冻
    "cust_merchant_no": None, #商户侧用户 id
    "balance_acc_no": None, #虚拟账户号
    "account_type": "01",  #00:内部账户01:余额账户02:积分账户 03:红包账户
    "frozenAmount": None, #冻结资金
    "frozenType": None, # 1 | 永久， 2 | 临时
    "implementFlag": None, #0 | 立即执行，1 | 定时执行
    "currency": None, #1:人民币 2:美元 3:港币 4:其他币种
    "frozenStartTime":None, #时间格式：yyyymmdd
    "frozenEndTime": None,
    "frozenReason": None,
    "thawrozenFlag":None, # 0 | 全部解冻， 1 | 部分解冻
    "businSerialNum":None, # 选择部分解冻时，需填冻结流水号
    "cust_type": "08", #企业
}

balance_money_change_request_dict_template_3_10 = { # 虚拟账户上账
    "cust_merchant_no": None, # 商户侧id
    "balance_acc_no": None, #虚拟账户号
    "mer_order_no": None, #商户交易流水号, 32位
    "amount": None,
    "currency": "CNY",
    "trade_remark": None,
    "origin_type": None, #1：上账2：下账（不填默认1）
}

withdraw_request_dict_template_3_11 = {
    "cust_merchant_no": None, #商户侧id
    "balance_acc_no": None, #虚拟账户号
    "mer_order_no": None, #商户交易流水号
    "amount": None,
    "currency": "CNY",
    "account_in": None,
    "name_in": None,
    "cust_no_in": None,
    "acct_flag": "1", #1：对私 2：对公
    "bank_short_name": None,
}

consumption_fee_dict_template_3_12 = {
    "mer_order_no": None, #商户交易流水号
    "cust_merchant_no": None, #商户侧id
    "balance_acc_no": None, #虚拟账户号
    "currency": "CNY",
    "cust_no_in":None, #入账方商户侧用户唯一 ID
    "account_in": None,   #入账方账号
    "amount": None,       #交易金额
    "poundage_amount": None, #手续费
    "trans_nature":"01", # 交易性质 01|正常交易 02|冲正交易 03|撤销交易 04|退款交易 05|补单交易 06|平账交易
    "purpose" : None,#用途 1001实物商品租购，1002虚拟商品购买，1003交通票务，1004活动票务订购，1005商业服务消费，1006生活服务类，
                     #    1099其他商家消费，2001水电煤缴费，2002税费缴纳，2003学校教育缴费，2004医疗缴费，2005罚款缴费，2006路桥通行缴费，
                     #    2007公益捐款，2099其他公共服务，3001基金理财申购，3002基金理财认购
}

cancel_consume_dict_template_3_13 = {
    "mer_order_no": None,      #商户交易流水号
    "cust_merchant_no": None,  #商户侧id
    "balance_acc_no": None,    #虚拟账户号
    "currency": "CNY",
    "original_trade_no": None, #原商户订单号
}

consume_reversal_dict_template_3_14 = {
    "mer_order_no":None,
    "cust_merchant_no": None,  # 商户侧id
    "balance_acc_no": None,  # 虚拟账户号
    "currency": "CNY",
    "original_trade_no": None, #原商户订单号
}

consume_refund_dict_template_3_15 ={
    "mer_order_no": None,
    "cust_merchant_no": None,  # 商户侧id
    "balance_acc_no": None,  # 虚拟账户号
    "currency": "CNY",
    "original_trade_no": None,  # 原商户订单号
    "amount" : None, # 退款金额
}

trade_detail_query_dict_template_3_16 = {
    "mer_order_no" : None, #商户交易流水号
    "order_no" : None, #平台交易流水号
}

consume_ledger_dict_template_3_17 = {
    "mer_order_no": None,
    "cust_merchant_out": None,  # 出账方商户侧id
    "account_out" : None , #出帐账户
    "currency": "CNY",
    "amount" : None , #金额
    "poundage_amount" : None , #手续费
    "trans_nature" : "01", #交易类型 属于正常交易故传 01
    "ledger_account_info" : None, # 分帐信息
    "fee_splitting_info": None # 分润信息
}

ledger_account_info_dict_template_3_17 = {
    "cust_merchant_in": None, # 商户侧id
    "account_in" : None,  #入账账户
    "cust_type" : None,   # 账户类型 00|个人，08|企业
    "ledger_amount" : None, # 分帐金额 函手续费
    "fee_amount" : None, # 手续费金额
}
fee_splitting_info_dict_template_3_17 = {
    "cust_merchant_in": None, # 商户侧id
    "account_in" : None,  #入账账户
    "cust_type" : None,   # 账户类型 00|个人，08|企业
    "splitting_amount" : None, # 分润金额，所有分润金额总和需等于手续费金额
    "guarantee": False, # 是否担保交易
}

confirmation_consumption_call_dict_template_3_18 = {
    "mer_order_no" : None,       #商户交易流水号
    "trade_no" : None ,          #交易流水号
    "cust_merchant_out" : None , #商户侧唯一id
    "cust_type_in" : None,       #会员类型 00：个人会员 08：企业会员
}

weak_certified_dict_template_3_19 = {
    "interface_type":"0", # 0,个人实名 2，企业实名
    "cust_merchant_no":None, #商户侧用户唯一 ID
    "cust_mobile" : None, #注册手机号
    "cust_name" : None, #姓名
    "cust_idt" : "0", #证件类型: 0:二代身份证
    "cust_id_no" : None, #证件号
    "ent_name" : None, #企业名称
    "enterprise_card_type" : None, # 企业证件类型
    "enterprise_registration_no" :None, # 企业证件号码
    "corporation_name" : None, #法人姓名
    "corporation_id_no" : None , #法人证件号码
}


transfer_dict_template_3_21 = {
    "mer_order_no":None,     #交易流水
    "cust_merchant_no":None, #出帐方唯一测id
    "name_out":None,         #出帐方姓名
    "balance_acc_no":None,   #出帐方虚户帐号
    "cust_no_in":None,       #入账方商户侧用户唯一 ID
    "name_in":None,          #入账方商户姓名
    "account_in":None,       #入账方商户账号
    "currency":"CNY",
    "amount":None,           #转账金额
    "acct_flag":None,        #1对公2对私

}

consume_dict_template_3_22 = {
    "mer_order_no":None ,      #商户交易流水号
    "cust_merchant_no" : None, #出账方商户侧用户唯一 ID
    "balance_acc_no" : None ,  #出账方商户姓名
    "name_out" : None ,        #出账方商户账号
    "currency": "CNY",
    "amount" : None,           #交易金额
    "purpose" : None,
}

send_msg_code_dict_template_4_1 = {
    "mer_order_no" : None,
    "cust_merchant_no" : None ,
    "cust_mobile" : None ,
    "operate_type" : None, #01-个人注册、02-个人实名、03-企业注册、04-企业实名、05-小微企业实名、11-充值、12-提现、 13-转账、14-消费、
                           #15-消费分账 16-提现手续费 17-消费手续费18-个人实名(仅实名)企业实名(仅实名)
    "trade_amount" : None, #交易金额
    "order_no" : None, #交易订单号
}
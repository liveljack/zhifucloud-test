# -*- coding: utf-8 -*-
import logging.config
import http.client
from datetime import datetime
import uuid
import json

from src.config_loader import ConfigLoader
from src.common_tools.cryptos.zhifucloud import Encryptor
from src.common_tools.errors.signature_fail_error import SignatureFailure
from src.common_tools.tool import Tool

class NetBroker:
    """
    net request broker for zhifu cloud
    """
    def __init__(self):
        config_loader = ConfigLoader()
        self.zhifu_configs = config_loader.loadConfigs()["zhifucloud"]
        self.mer_id = self.zhifu_configs["mer_id"]
        self.app_id = self.zhifu_configs["app_id"]
        self.app_key = self.zhifu_configs["app_key"]

        self.api_ip = self.zhifu_configs["api_ip"]
        self.api_port = self.zhifu_configs["api_port"]
        self.api_url = self.zhifu_configs["api_url"]

        self.logger = logging.getLogger(config_loader.loadConfigs()["default-logger"])
        self.encryptor = Encryptor("RSA")
        self.tool = Tool()


    def request_url(self,url, headers, bodies):
        """
        request zhifu cloud for specified service, including the md5 signature
        :param url: the target url
        :param headers: dict of headers
        :param bodies:
        :return:
        """

        assert (type(headers) is dict) #make sure are dicts
        assert (type(bodies) is dict)

        #remove the None content from headers and bodies
        headers = self.tool.removeNoneItemfromDict(headers)
        bodies = self.tool.removeNoneItemfromDict(bodies)

        # make the sign
        (sign_content,sign) = self.encryptor.sign(headers,bodies)

        request_content = {"body": bodies, "sign": sign,"head":headers }

        # encoded_body = urllib.parse.urlencode(request_content,encoding="utf-8")
        # print(f"encoded request body: {encoded_body}")
        request_content_json = json.dumps(request_content)
        self.logger.info(f"To be requested content: {request_content}")

        cipher_content = self.encryptor.encrypt(request_content_json)
        self.logger.debug(f"The to be requested ENCRYPTION content: {cipher_content}")

        # doing the request
        conn = http.client.HTTPConnection(self.api_ip, self.api_port)
        conn.request("POST", url, cipher_content)
        response = conn.getresponse()
        resp_content = response.read()
        self.logger.debug(f"The response content is: {resp_content}")

        resp_plain = self.encryptor.decrypt(resp_content)
        self.logger.info(f"The DECRYPTED response is: {resp_plain}")

        # check the sign of response
        resp_json = json.loads(resp_plain)
        resp_head = resp_json["head"]
        resp_body = resp_json["body"]
        resp_sign = resp_json["sign"]

        (sign_content,sign) = self.encryptor.sign(resp_head,resp_body,"response")
        self.logger.debug(f"The signature of calculated is {sign}, the sign in content is {resp_sign}")

        if sign != resp_sign:
            raise SignatureFailure(f"The signature {sign} is different from content {resp_sign}",resp_head, resp_body, resp_sign, sign)

        return resp_plain

    def request(self, headers, bodies):
        return self.request_url(self.api_url,headers,bodies)

    def request_endpoint(self, service_endpoint, request_sequence, bodies):
        """
        request for service endpoint, generate headers dict
        :param service_endpoint:
        :param request_sequence:
        :param bodies:
        :return:
        """

        headers = {
            "service": service_endpoint,
            "mer_id": self.mer_id,
            "app_id": self.app_id,
            "req_sn": request_sequence,
            "timestamp": datetime.now().strftime("%Y%m%d%H%M%S"),
        }

        self.logger.debug(f"The to be requested header: {headers}")
        self.logger.debug(f"The to be requested body: {bodies}")

        return self.request(headers, bodies)


# -*- coding: utf-8 -*-
import base64
import os

class PictureTool:


    def jpg_to_base64(self,jpg_url):

        assert type(jpg_url) is str
        assert jpg_url[-4:].lower() == ".jpg"
        assert os.path.exists(jpg_url)

        with open(jpg_url, "rb") as f:
            base64_data = base64.b64encode(f.read())

        return str(base64_data)
# -*- coding: utf-8 -*-
class Tool:
    def __init__(self):
        pass

    def fillDictContent(self,tar_dict,src_dict, specified_keys = []):
        """
        tar_dict中的字段，如果为空或者长度为零，并且src_dict存在同名字段，则用src_dict中的填充
        如果key在specified_keys列表里面，则也一并修改
        :param tar_dict:
        :param src_dict:
        :param specified_keys: to be filled field names
        :return: the shallow copy of tar_dict, should not ruin the tar_dict itself
        """
        assert type(specified_keys) is list #make sure the specified keys is a name list
        filled_dict = tar_dict.copy()

        for key in list(tar_dict):
            value = tar_dict[key]
            #只有非空字段才能填充
            if (value == None) or ((type(value) is str) and (len(value)==0)):
                if key in list(src_dict):
                    filled_dict[key] = src_dict[key]

            #或者在指定列表中
            if key in specified_keys:
                if key in list(src_dict):
                    filled_dict[key] = src_dict[key]

        return filled_dict

    def removeNoneItemfromDict(self, src_dict):
        """
        remove the None item from src dict, do NOT change src, shallow copy to target dict
        :param src_dict:
        :return:
        """

        assert type(src_dict) is dict
        tar_dict = src_dict.copy()

        for key in list(tar_dict):
            if tar_dict[key] == None:
                del tar_dict[key]

        return tar_dict
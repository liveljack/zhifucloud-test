# -*- coding: utf-8 -*-
class SignatureFailure(Exception):

    def __init__(self,message,heads_dict,bodies_dict,sign_in_text,sign_calculated):
        """
        the signature calculated is different from the specified one
        :param message:
        :param heads_dict:
        :param bodies_dict:
        :param sign_in_text:
        :param sign_calculated:
        """
        self.message = message
        self.heads_dict = heads_dict
        self.bodies_dict = bodies_dict
        self.sign_in_text = sign_in_text
        self.sign_calculated = sign_calculated

    def __str__(self):
        str = f"SignatureFailed {self.message}, should be (calculated) {self.sign_calculated} but is (in text) {self.sign_in_text}"
        return str

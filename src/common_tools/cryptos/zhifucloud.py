# -*- coding: utf-8 -*-
import logging.config
import base64
import M2Crypto
import hashlib
import re
import json

from src.config_loader import ConfigLoader
from src.common_tools.tool import Tool

class Encryptor:
    """
    the cryptor for zhifu cloud
    ony RSA supported now, with the public key
    NOTE: since the rsa use the 1024 bits for keys, the padding length is hard-coded 117, and 128 for encryption and decryption separately
    """
    def __init__(self, crypto_algorithm):
        self.crypto_algorithm = crypto_algorithm.lower()

        assert(self.crypto_algorithm=="rsa"); #only support rsa mode

        config_loader = ConfigLoader()
        self.pub_key_file = config_loader.loadConfigs()["zhifucloud"]["rsa_pub_key_file"]
        self.app_key = config_loader.loadConfigs()["zhifucloud"]["app_key"]

        self.logger = logging.getLogger(config_loader.loadConfigs()["default-logger"])
        self.tool = Tool()

    def decrypt(self, cipher_content):
        """
        encrypt the content, with RSA, and encrypt with public key
        :param cipher_content:
        :return: the plain content in utf-8 encode
        """
        plain_text = ""
        if self.crypto_algorithm == "rsa":
            rsa_pub = M2Crypto.RSA.load_pub_key(self.pub_key_file)
            ctxt_pri = base64.b64decode(cipher_content)  # 先将str转成base64

            whole_content = bytes()

            while ctxt_pri:
                input = ctxt_pri[:128]
                ctxt_pri = ctxt_pri[128:]
                out = rsa_pub.public_decrypt(input, M2Crypto.RSA.pkcs1_padding)  # 解密
                whole_content += out


            self.logger.debug('公钥明文:%s' % str(whole_content, encoding="utf-8"))
            plain_text = whole_content.decode("utf-8")

        return plain_text


    def encrypt(self, plain_content):
        """
        decrypt the content with RSA public key
        :param plain_content: the encrypted content, in base 64 format
        :return: plain_text
        """
        cipher_text = ""

        if self.crypto_algorithm == "rsa":
            rsa_pub = M2Crypto.RSA.load_pub_key(self.pub_key_file)
            plain_content = plain_content.encode("utf-8")
            len_content = len(plain_content)
            offset = 0
            params_lst = []

            # 这里的方法选择加密填充方式，所以在解密的时候 要对应。
            while len_content - offset > 0:
                if len_content - offset > 117:
                    params_lst.append(rsa_pub.public_encrypt(plain_content[offset:offset + 117], M2Crypto.RSA.pkcs1_padding))
                else:
                    params_lst.append(rsa_pub.public_encrypt(plain_content[offset:], M2Crypto.RSA.pkcs1_padding))

                offset += 117

            # concat all the contents
            whole_content = bytes()
            for param in params_lst:
                whole_content = whole_content + param

            self.logger.debug(f"The type content is: {whole_content}")
            cipher_text = base64.b64encode(whole_content)

        return cipher_text


    def sign(self,heads,bodies,direction="request"):
        """
        contcat the contents of heads and bodies, and generate the sign
        :param heads:
        :param bodies:
        :param direction:(request, or response), make request as default
        :return: (sign_content, signature)
        """
        # remove the None content from headers and bodies
        heads = self.tool.removeNoneItemfromDict(heads)
        bodies = self.tool.removeNoneItemfromDict(bodies)

        sign_content = ""
        sign_dict = heads.copy()  # combine the headers and bodies
        sign_dict.update(bodies)

        # add headers one by one, and add to sign content
        for key in sorted(sign_dict):
            content_value = str(sign_dict[key])

            # if key == "reqListDateMap":
            #     content_value = self.get_content_display(sign_dict[key])

            sign_content = sign_content + key + "=" + content_value + "&"

        sign_content = sign_content + "key=" + self.app_key
        #since zhifucloud use different ways to make up sign content
        if direction=="response":
            # sign_content = sign_content.replace(' ', '')
            sign_content = sign_content.replace("'","")
            sign_content = sign_content.replace(': ', '=')

        # 删除多余空格
        # def _repl_trail_space(matched_ob):
        #     space_p = matched_ob.group(0)
        #     return re.sub(r"\s+", "", space_p)
        # sign_content = re.sub(r'[:",](\s+)[",:]', _repl_trail_space, sign_content)

        encrypt_md5 = hashlib.md5()
        encrypt_md5.update(sign_content.encode("utf-8"))
        sign = encrypt_md5.hexdigest()

        self.logger.info(f"The signature [{sign}] for the signed content is:{sign_content}")

        return (sign_content, sign)



    def get_content_display(self,content):
        """
        获取签名所用的字段显示，如果是列表，需要对列表排序
        :param content:
        :return:
        """
        display_content = ""
        content_json = json.loads(content)
        if (type(content_json) is dict) or (type(content_json) is list):
            display_content = json.dumps(content_json, sort_keys=True , ensure_ascii=False)
        else:
            display_content = str(content_json)

        display_content = display_content.replace(': ', '=')
        display_content = display_content.replace('"', '')
        return display_content
# -*- coding: utf-8 -*-
import yaml
import os.path
import logging.config

class ConfigLoader:
    configs = {} #global configs
    config_paths = ["src/config/zhifu-main.yml", "src/config/log-config.yml"]

    def loadConfigs(self):
        """
        load global system configurations and the logging config
        :return:
        """
        #load only once
        if len(self.configs) == 0:
            for con_path in self.config_paths:
                cons = yaml.load(open(con_path,"r",encoding='utf-8'))
                self.configs.update(cons)

            #load the logging config
            logging.config.dictConfig(self.configs["logging"])

        return self.configs

    def __str__(self):
        return self.configs;
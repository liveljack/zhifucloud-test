# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
setup(
    name="zhifucloud-api-test",
    version="0.1",
    packages=find_packages(),

    install_requires=['Faker>=0.9.1', 'PyYAML>=3.13', 'M2Crypto>=0.30.1', 'pytest>=3.8.1'],

    include_package_data=True,
    zip_safe=True,

    package_data={
        # If any package contains *.txt files, include them:
        'src': ['*.py', '*.pem', '*.yml'],
        'tests': ['*.py', '*.pem', '*.yml'],
        '': ['setup.py']
    },

    entry_points={
        'console_scripts': [
            'api-test = tests.zhifucloud_api_test:test_open_account',
        ],
    },




    # metadata to display on PyPI
    author="Jacky Wang",
    author_email="84305166@qq.com",
    description="The api test module for zhifucloud service",
)
